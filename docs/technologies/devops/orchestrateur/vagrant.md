# Vagrant

- [Vagrant](#vagrant)
  - [Ressources](#ressources)
  - [Les commandes basiques](#les-commandes-basiques)
  - [Install Jenkins VM - Cf. 1.9 folder](#install-jenkins-vm---cf-19-folder)
    - [Provisionning jenkins VM](#provisionning-jenkins-vm)
  - [Bonnes pratiques de prod](#bonnes-pratiques-de-prod)

## Ressources

- [install jenkins via packages](https://pkg.jenkins.io/debian-stable/)
- [install docker registry](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-22-04)
- [install gitlab](https://about.gitlab.com/install/#debian)

## Les commandes basiques

Vagrant ne peut s'exécuter que s'il y a un fichier Vagrantfile.

```shell
vagrant up # download image & run
vagrant up <vm_name> # lance une VM spécifique
vagrant snapshot push # faire des snapshots
vagrant snapshot list # liste les snaps
vagrant snapshot pop # charger un snap
vagrant halt #arrêt la ou les machines
vagrant destroy # détruire la ou les machines
vagrant status # liste les machines
cat Vagrantfile | grep -ri "ip:" # liste les ip
```

## Install Jenkins VM - Cf. 1.9 folder

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # p1jenkins server
  config.vm.define "p1jenkins-pipeline" do |p1jenkins|
    p1jenkins.vm.box = "debian/buster64"
    p1jenkins.vm.hostname = "p1jenkins-pipeline"
    p1jenkins.vm.box_url = "debian/buster64"
    p1jenkins.vm.network :private_network, ip: "192.168.56.2"
    p1jenkins.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
      v.customize ["modifyvm", :id, "--memory", 3072]
      v.customize ["modifyvm", :id, "--name", "p1jenkins-pipeline"]
      v.customize ["modifyvm", :id, "--cpus", "2"]
    end
    config.vm.provision "shell", inline: <<-SHELL
      sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config    
      service ssh restart
    SHELL
    p1jenkins.vm.provision "shell", path: "install_p1jenkins.sh"
  end
end
```

### Provisionning jenkins VM

```shell
#!/bin/bash

## ? Install p1jenkins

IP=$(hostname -I | awk '{print $2}')

echo "START - install jenkins - "$IP

echo "[1]: install utils & ansible"
apt-get update -qq >/dev/null
apt-get install -qq -y git sshpass wget ansible gnupg2 curl

echo "[2]: install java & jenkins"
echo "[3]: add jenkins key to the system"
sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
    https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
echo "[4]: add a Jenkins apt repository entry"
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
    https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null
echo "[5]: install jdk & jenkins"
sudo apt-get update -qq >/dev/null
sudo apt-get install -y fontconfig openjdk-11-jre jenkins
sudo systemctl enable jenkins
sudo systemctl start jenkins


echo "[6]: ansible custom"
sed -i 's/.*pipelining.*/pipelining = True/' /etc/ansible/ansible.cfg
sed -i 's/.*allow_world_readable_tmpfiles.*/allow_world_readable_tmpfiles = True/' /etc/ansible/ansible.cfg

echo "[7]: install docker & docker-compose"
curl -fsSL https://get.docker.com | sh; >/dev/null
usermod -aG docker jenkins # authorize docker for jenkins user
curl -sL "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo "[8]: use registry without ssl"
echo "
{
 \"insecure-registries\" : [\"192.168.56.5:5000\"]
}
" >/etc/docker/daemon.json
systemctl daemon-reload
systemctl restart docker

echo "END - install jenkins"
```

## Bonnes pratiques de prod

- Avoir un jenkins isolé dans vlan propre
  - avec des slaves pour les projets/micro services afin de ne pas surcharger le master
- pour assurer les mise à jour des applicatifs (mysql, jenkins, ansible etc...) il faudrait un dépot local afin que les machines n'accèdent pas à l'extérieur
- assurer de la réplication des datas
- mettre un reverse-proxy devant les applicatifs pour ne pas taper dedans