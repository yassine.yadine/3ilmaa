# -*- coding: utf8 -*-

import random

quotes = [
    "Ecoutez-moi, Monsieur Shakespeare, nous avons beau être ou ne pas être, nous sommes !", 
    "On doit pouvoir choisir entre s'écouter parler et se faire entendre."
]

characters = [
    "alvin et les Chipmunks", 
    "Babar", 
    "betty boop", 
    "calimero", 
    "casper", 
    "le chat potté", 
    "Kirikou"
] 

print(quotes)

# déclaration d'une fonction
def get_random_item_in(my_list):
    rand_numb = random.randint(0, len(my_list) - 1)
    item = my_list[rand_numb] # get a quote from a list
    return item # return the item

# appel d'une fonction
get_random_item_in(quotes);
'''
if ret is None :
    print("retour null");
else :
    print("retour non null : {}".format(ret));
'''
