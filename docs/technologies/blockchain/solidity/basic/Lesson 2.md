# Lesson 2

## chp2. Mappings

Mapping declaration is a Key-Value map. See the example below :

```
mapping (uint => address) public zombieToOwner;
mapping (address => uint) ownerZombieCount;
```

## chp3. msg.sender

msg.sender is a solidity global variable, using private key containing person/contract address who has executed this function.

Below, we associate the new created zombie to the one who called this function.

```
zombieToOwner[id] = msg.sender;
```

------------------------------------------------------------
Because we added a zombie to the collection, we can increment the ownerCount

```
ownerZombieCount[msg.sender] ++ ;
```

## chp4. Require

Require just one zombie created. Will return an error if check failed.        

```
require(ownerZombieCount[msg.sender] == 0);
```

## chp5. Heritage

Heritage declaration allows  organize your code and the business logic.

Here is an exemple.

```
contract ZombieFeeding is ZombieFactory {}
```

<u>Explanation</u>

BabyDoge is accessing *anotherCatchphrase* & *catchphrase* methods.

```
contract Doge {  
	function catchphrase() public returns (string) {    			return "So Wow CryptoDoge";  
	} 
} 

contract BabyDoge is Doge {  
	function anotherCatchphrase() public returns (string) {    return "Such Moon BabyDoge";  
	} 
}
```

## chp6. Importation

We can use *import* key word to import another sol class in the current class.

./ means in the same directory used.

```
import "./zombiefactory.sol";
```

After this chapter, we need reals solidity files using the right naming.  This is why, a sub directory containing the project have been created in git.

## chp7. Storage & Memory

Here we are talking about how data is stored in blockchain.

```
function feedAndMultiply(uint _zombieId, uint _targetDna) public {

	// require usage to check if the consumer is the zombie owner
	// zombieToOwner is a mapping that accept a zombie ID
	// and give you in return the address of his owner.
    require (msg.sender == zombieToOwner[_zombieId]);

    // storage keyword usage to store the current definitely zombie into blockchain
    Zombie storage myZombie = zombies[_zombieId];
}
```

## chp8-9. Internal & External functions

We've already seen public and private functions. There are also two others accessors : internal & external.

- internal -> if a contract named "B" inherits from an other contract "A". All functions declared *internal* in "A" could be called in "B". Private functions could not.
- external -> functions declared as external could only be called out from  of the contract.

## chp10-11. Interfaces

We can call & execute external contracts by implementing interfaces. Then, we have just to fire the interface to execute the external contract.

External contract example :

```
contract NumberInterface {
	function getNum(address _myAddress) public view returns (uint);
}
```

Interface implementation & call

```
contract MyContract {
	address NumberInterfaceAddress = 0xab38...  *// ^

    L'adresse du contrat FavoriteNumber sur Ethereum
    NumberInterface numberContract = NumberInterface(NumberInterfaceAddress)
	// Maintenant `numberContract` pointe vers l'autre contrat

    function someFunction() public {
        //Nous pouvons maintenant appeler `getNum` à partir de ce contrat :
        uint num = numberContract.getNum(msg.sender);
        // ...et faire quelque chose avec ce `num`*
    }
}
```

