# Essentials of Cloud Computing 

##  Essentials of Cloud Computing

#### Motivations du "move to cloud"

- Investissement massif pour monter l'infra/ data center
- Temps d'installation énorme pour monter l'infrastructure
- Coût de la maintenance important
- Incapacité à gérer la sur ou sous utilisation des ressources
- En raison de tous ces paramètres, le ROI (Return Over Investissment = retour sur investissement) est réduit

#### Problématique des infras traditionnelles

![image-20220924193935343](img/image-20220924193935343.png)

#### Bénéfices du Cloud

![image-20220924194314459](img/image-20220924194314459.png)

![image-20220924194535220](img/image-20220924194535220.png)

#### Features du Cloud

![image-20220924194759338](img/image-20220924194759338.png)



## Cloud deployments models

#### Les différents modèles

![image-20220924194948917](img/image-20220924194948917.png)

#### Public Cloud

C'est un fournisseur qui assure la disponibilité du service au grand public via une connexion Internet.

- Multi-tenant
- Haute scalabilité
- Résilient
- Zéro maintenance

#### Private Cloud

Ce sont typiquement les entreprises qui utilisent ce type de services pour leurs employés, fournisseurs, clients etc...

Le service est utilisé via un fournisseur ou alors le service est installé sur site.

- Single tenant

- Localisation de la data

- Sécurité, autorités, lois imposant une règle sur les données/services

- Sécurité améliorée

#### Community Cloud

Ce sont bien souvent des conglomérats d'entreprises qui proposent ce genre de service

- infra partagée
- effort collaboratif
- multi-tenant
- la communauté gère la gouvernance

#### Hybrid Cloud

C'est une solution regroupant un ou plusieurs modèles Cloud. Les deux cloud peuvent échanger des données. C'est particulière adapté lorsque la collaboration est plus importante que la sécurité. Ce modèle offre de la flexibilité.

#### Vue globale

![image-20220924200540194](img/image-20220924200540194.png)

## Cloud services models

#### Software as a Service (SaaS)

- applications prêtes à l'emploi
- pas de visibilité sur le backend
- accès via un navigateur Web ou appli mobile
- facturé à la souscription du service

Exemples : Office 365, Google apps, Microsoft Teams, Slack channel

#### Platform as a Service (PaaS)

- runtime et la plateforme (Java, Apache, .NET, Python etc...) fournis par le fournisseur Cloud
- Pas besoin de gérer les plateformes
- moins de contrôle sur l'infra
- contrôle sur le code applicatif déployé sur la plateforme

Exemples : App Cloud Salesforce, Google App Engine, OpenShift RedHat, Oracle Cloud

#### Infrastructure as a Service (IaaS)

- CPU, espace disque, réseau à disposition
- Contrôle sur l'OS et le stockage des données
- déploiement à distance

Exemples : Elastic Compute Cloud EC2, Microsoft Azure, Google Compute Engine.

#### Vue globale

![image-20220924203130317](img/image-20220924203130317.png)

## Serverless services (FaaS)

Il s'agit ici d'un trigger qui permet depuis un évènement (click sur un bouton) de déclencher une fonction (rappel du concept de procédure stockée).

- Plus besoin de gérer l'infra :
   - apporter les ressources nécessaires
   - patcher l'OS
- Réduit le Total Cost of Ownership (TCO)
- haute disponibilité et tolérance aux pannes
- Est aussi appelé Function as a Service (FaaS)

#### Exemple #1

![image-20220925152222978](img/image-20220925152222978.png)

#### Exemple #2

![image-20220925152344633](img/image-20220925152344633.png)

#### Vue globale

![image-20220925152538133](img/image-20220925152538133.png)

## Major Cloud service providers

#### Le top 3 des CSP (Cloud Services Provider)

![image-20220925152955136](img/image-20220925152955136.png)

#### Le coût d'utilisation

Les csp ont développé des outils pour calculer leur prix

- AWS TCO calculator
- AWS simple Monthly calculator
- Google CloudPlatform Calculator
- NetApp Azure calculator
- Cloud storage calculator
- Unigma calculator

## Virtualization

#### Motivations pour la virtualisation

- permet de lancer des apps sur peu de machines physiques
- réduit le TCO
- décoréllation la charge de travail sur l'infra physique

![image-20220925153956474](img/image-20220925153956474.png)

#### Architecture

![image-20220925154149909](img/image-20220925154149909.png)

#### Les types de virtualisation

![image-20220925201427220](img/image-20220925201427220.png)

## Containers

#### Motivations

![image-20220925201644487](img/image-20220925201644487.png)

#### Architecture d'un container

![image-20220925201759550](img/image-20220925201759550.png)

#### Bénéfices de la containérisation

- Rapidement scalable
- Juste utilisation des ressources
- Portabilité accrue des applis containérisée
- Augmente l'efficacité

#### Containers vs VM

![image-20220925202144138](img/image-20220925202144138.png)

#### Technologie Docker

- Open source
- Linux & Windows
- Exécutable partout
- Docker CLI, REST API
- Docker image
- Docker File

#### Orchestration

- Automatise la gestion des containers
- S'intègre avec CI/CD
- Permet de gérer : 
  - la disponibilité des containers
  - Le load balancing & les routes
  - l'échange sécurisé entre les containers

Exemple de technos d'orchestration : Docker Swarm, Kubernetes, Apache Mesos, Amazon EKS, GKE

## Cloud Native Application Developpment (CNAD)

C'est une approche qui permet de développer une application en prenant compte depuis le début tous les éléments suivants :

![image-20220925202752087](img/image-20220925202752087.png)

#### Micro services

Au lieu d'avoir un core monolythique, il faut privilégier les micro services :

- Ils sont indépendants les uns des autres
- Ils sont tolérants aux pannes puisque déployer sur des instances différentes
- Permettent la livraison continue
- Ils sont très bien supportés par les containeurs
- Ils sont scalables et réutilisables

![image-20220926224435628](img/image-20220926224435628.png)

#### Stateful vs Stateless architecture

Il faut privilégier les routes génériques afin de pouvoir scaler rapidement :

![image-20220926224756774](img/image-20220926224756774.png)

#### DevOps

- Collection d'outils et de pratiques
- Permet d'accélérer le cycle de développement d'un produit
- Permet l'agilité dans le développement software
  - Détecte les problèmes tôt avant qu'ils ne coûtent trop cher
- Réduit le time to market d'un produit

#### CI / CD

- automatise le process de création de release
- dès que le code change, le pipeline release est enclenché automatiquement
- permet de nombreux déploiements dans une courte période de temps

![image-20220926225735600](img/image-20220926225735600.png)

#### Containers

- les containers supportent l'approche cloud native
- Le container est l'unité atomique d'une "compute capacity"
- démarrage rapide et scale facile

## Cloud migration

Les organisations migrent leur application pour réduire le TCO. Et pour se faire, ces dernières vont suivre 3 étapes : découverte, inventaire et migration

#### Discovery

C'est une étape cruciale puisqu'elle est la base sur laquelle une organisation va établir sa stratégie de migration. Ici, l'enjeu est de :

- modéliser la vue d'ensemble de l'infra
- déterminer tous les flux entrants & sortant et leur utilisation
- découverte des applications déployées sur les serveurs + leur interface
- comprendre les caractéristiques et dépendances de chaque application

#### Assessment

Ici, il faut faire un inventaire afin de déterminer les artefacts à inclure dans le plan de migration

- inventaire des serveur
- architecture de l'infra
- portfolio applicatif
- inventaire TCO afin de déterminer les coûts de l'existant
- évaluer la préparation de la migration

#### Stratégies de migration

- re-host : aucun changement applicatif : on prend l'appli et on la déploie sur un cloud
- re-platform : on va apporter des optimisations afin de profiter de l'environnement cloud
- refactor : apporter des changements structurels pour vraiment s'inplanter dans le cloud
- re-architect : on va repenser l'application tout en réutlisant l'existant afin d'augmenter
  - user expérience
  - disponibilité
  - performances
  - accessibilité
  - maintenance
  - fonctionnalités
- retire : on procède au démantèlement de l'application car il y en a une autre qui la remplace
- retain : finalement, on décide de ne rien migrer et de rester sur la solution existante

#### Migration

C'est un process itératif 

![image-20220926232048153](img/image-20220926232048153.png)

#### Outils pour la migration

![image-20220926232231932](img/image-20220926232231932.png)

### Cloud security

#### Les risques dans le cloud

- accès non autorisés
- stockage des données
- transfert de données 
- sécurité de l'infra physique du provider

#### Corporate datacenter vs Cloud security

![image-20220930201033957](img/image-20220930201033957.png)

#### Responsabilités de sécurité

![image-20220930201324644](img/image-20220930201324644.png)

#### Fonctionnalités de sécurité offertes par le CSP

- il est possible de configurer les règles corporate dans les services cloud
- Contrôler l'accès aux ressources
- Encrypter
- Gérer la compliance
- Monitoring & auditing

#### Bonnes pratiques

- Bien comprendre le partage de responsabilités
- toujours  chiffrer les canaux, données etc...
- ne pas laisser les identifiants de connexion sans chiffrement
- gérer les accès non authentifiés
- Monitorer en permanence

### Latest trends in Cloud

#### Multi-Cloud

L'objectif ici est de répliquer exactement la même infra sur différents CSPs

- réduit le couplage fort envers un fournissuer
- augmente la résilience et la redondance
- flexibilité
- automatisation
- prix compétitifs, on fait travailler la concurrence

![image-20220930202938826](img/image-20220930202938826.png)

#### Poly Cloud

Ici, il s'agit de sélectionner certaines fonctionnalités d'un CSP et d'autres sur un autre CSP. L'enjeu est de choisir les fonctionnalités avec le meilleur ratio prix/qualité/sécurité

![image-20220930203220319](img/image-20220930203220319.png)

##### Avantages & inconvénients 

![image-20220930203421670](img/image-20220930203421670.png)

#### IA &Machine learning in Cloud

Les objectifs sont les suivants :

- automatiser les tâches rébarbatives ayant peu de valeur ajoutée à ce qu'elles soient réalisées par un humain.
- optimiser les processus métiers
- dirige l'innovation
- améliore la productivité des équipes libérées des tâches débiles :p
- améliore la sécurité

#### Analyses des données

- récupérer les stories et profiler les comportements pour esquisser des chémas prédictifs
- améliore les choix décisionnels pour le top management
- permet de découvrir/développer de nouvelles tendances

#### IoT & informatique de pointe

- automatisation
- objets connectés & contrôle de ces objets
- renforce l'analyse des données et apporte une source de données supplémentaire

#### DevSecOps

L'idée est de verrouiller l'infra, services et applis afin de ne pas être victime de rupture de services

- intégrer la sécurité au DevOps
- Automatiser la sécurité
- Chiffrer au maximum ce qui peut l'être -> la cryptographie est clef et est à maîtriser
- Mettre au point des APIs securisée

#### Autres domaines impactés par le Cloud

![image-20220930205247508](img/image-20220930205247508.png)

