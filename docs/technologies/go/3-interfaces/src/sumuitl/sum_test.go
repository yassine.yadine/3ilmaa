package main

import "testing"

func TestSum(t *testing.T) {
	tables := []struct {
		opa, opb, result int
	}{
		{1, 1, 2},
		{1, 2, 3},
		{2, 2, 4},
		{5, 2, 7},
	}

	for _, table := range tables {
		total := Sum(table.opa, table.opb)
		if total != table.result {
			t.Errorf("Sum of (%d+%d) was incorrect, got: %d, want: %d.", table.opa, table.opb, total, table.result)
		}
	}
}
