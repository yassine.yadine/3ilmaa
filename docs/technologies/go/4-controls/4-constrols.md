# Controls

## Ressources

- [Interchain Academy](https://ida.interchain.io/tutorials/4-golang-intro/4-control.html)

## if

```go
if boolean_expression_1 {
    // execute, if boolean_expression_1 is true
} else if boolean_expression_2 {
    // execute, if boolean_expression_1 is false but boolean_expression_2
    // is true
// } else if ...
} else if boolean_expression_n {
    // execute, if boolean_expression_1, ... , boolean_expression_(n-1) are
    // false, but boolean_expression_n is true
} else {
    // execute, if boolean_expression_1, ... , boolean_expression_n are false
}

```

Ou encore

```go
if s := 10%2; s==0 {
    fmt.Println(s)
}

// check s'il y a une erreur à gérer
if err != nil {
    fmt.Printf(err)
}

```

## Switch

### Sur les valeurs

```go
switch expression {
    case value_1:
        // do something if expression is equal to value_1
    case value_2:
        // do something if expression is equal to value_2
    // ...
    case value_n:
        // do something if expression is equal to value_n
    default:
        // do something if value_1 ... value_n does not match expression
}
```

### Switch ans valeurs -> if

```go
switch {
    case boolean_expression_1:
        // if true is equal to boolean_expression_1
        // which is the same as if boolean_expression_1 is true.
    case boolean_expression_2:
        // else if
    // ...
    case boolean_expression_n:
        // else if
    default:
        // else
}
```

### Switch sur les types

```go
var i interface{}
switch i.(type) {
    case bool:
        // if i has type bool
        fmt.Printf("Value of i is %v\n", i.(bool))
    case int32, in64:
        // ...
    // ...
    default:
        // i has another type
}
```

## For

```go
for init_statement; condition_expression; post_statement {
    // do something
}
```

### Range

```go
for _, table := range tables {
  total := Sum(table.opa, table.opb)
  if total != table.result {
   t.Errorf("Sum of (%d+%d) was incorrect, got: %d, want: %d.", table.opa, table.opb, total, table.result)
  }
}
```
