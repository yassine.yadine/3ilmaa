package main

import "fmt"

func isMayor(age int) bool {
	if age >= 18 {
		return true
	}
	return false
}

func swap(x, y string) (r1 string, r2 string) {
	r1, r2 = y, x
	return
}

func main() {
	fmt.Println(isMayor(17))

	stra, strb := "chaine a", "chaine b"
	fmt.Println(stra, strb)
	stra, strb = swap(stra, strb)
	fmt.Println(stra, strb)
}
