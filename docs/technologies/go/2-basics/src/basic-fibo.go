package main

import "fmt"

func fibonacci() func() int {
	x, y := 0, 1
	return func() int {
		x, y = y, x+y
		return x
	}
}

func loop(n int, f func() int) {
	if n > 0 {
		fmt.Println(f())
		loop(n-1, f)
	}
}

func main() {
	f := fibonacci()
	loop(3, f)
	loop(2, f)
	loop(4, f)
	loop(1, f)
}
