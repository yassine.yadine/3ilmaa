# Go - basics

- [Go - basics](#go---basics)
  - [Ressources](#ressources)
  - [Numbers](#numbers)
    - [Les flottants (décimaux)](#les-flottants-décimaux)
    - [Autres types numériques](#autres-types-numériques)
  - [Strings](#strings)
  - [Booleans](#booleans)
  - [Type declaration](#type-declaration)
    - [Les constantes](#les-constantes)
  - [String formatting](#string-formatting)
  - [Functions](#functions)
    - [Closures](#closures)
  - [Methods](#methods)
  - [Pointer](#pointer)
    - [Pointeurs sur les méthodes](#pointeurs-sur-les-méthodes)

## Ressources

- [Interchain academy](https://ida.interchain.io/tutorials/4-golang-intro/2-basics.html)
- [Go by example](https://gobyexample.com/)

"Go est un langage à typage statique (les types des variables sont connus lors de la compilation et doivent être spécifiés expressément par le programmeur)"

## Numbers

- **unsigned** (non signé) : permet de stocker que des nombres positifs
- **signed** (signé): permet de stocker des nombres positifs et négatifs

![image-20220123102933613](./img/image-20220123102933613.png)

### Les flottants (décimaux)

![image-20220123104543510](img/image-20220123104543510.png)

### Autres types numériques

![image-20220123104635259](img/image-20220123104635259.png)

## Strings

En Go, les chaînes de caractères sont des séquences d'octets en mode read-only et sont donc immuables. Il sont codés par défaut en UTF-8.

## Booleans

C'est un bit représentant true ou false

## Type declaration

Le nom de la variable est déclarée avant son type.

```go
// première déclaration
var s string = "initial"

// seconde déclaration - inférence dynamique de type
s := "initial"

// troisième déclaration
var (
    a, b int
    s string
    c complex64
)

// quatrième déclaration
var a, b int
var s string
var c complex64
```

### Les constantes

Il faut utilise le mot clef **const** au lieu de **var**

```go
const hello = "Hello, World!"


// le code suivant fonctionne
const number = 2
var f float64 = number

// le code suivant ne fonctionne pas
const number = 2
var f float64 = number
```

## String formatting

- %v for a value, which will be converted into a string with default - options.
- %T for the type of a value
- %x for the hex encoding
- %d for integer
- %f for float, %e and %E for scientific notation
- %s for string
- %p for the pointer address of the variable

```go
package main
import "fmt"
func main() {
    a, b := 2, 3
    c := float64(a + b)
    fmt.Printf("%v + %v = %f = %v, stored as %T", a, b, c, c, c)
}

// will print
2 + 3 = 5.000000 = 5, stored as float64
```

## Functions

Les fonctions peuvent prendre un ou plusieurs arguments et peuvent renvoyer 0 ou plusieurs arguments.

```go
// principe
func myFunc(v1, v2 type12, v3 type3, v4 type3,....) (ret1 returntype1, ret2 returntype2, ...) {
    return
}

// exemple d'un swap
func swap(x, y string) (string, string) {
    return y, x
}

// ou encore
func swap(x, y string) (r1 string, r2 string) {
    r1, r2 = y, x
    return
}
```

### Closures

```go
package main

import "fmt"

/* cette fonction renvoie une fonction anonyme qui 
  utilisé comme un param, stocke ses variable locales => closure
*/
func fibonacci() func() int {
  x, y := 0, 1
  return func() int {
    x, y = y, x+y
    return x
  }
}

func loop(n int, f func() int) {
  if n > 0 {
    fmt.Println(f())
    loop(n-1, f)
  }
}

// 
func main() {
  f := fibonacci()
  loop(3, f)
  loop(2, f)
  loop(4, f)
  loop(1, f)
}
```

## Methods

Les méthodes s'appliquent sur des types. Go n'est pas un langage objet.
Les méthodes sont des fonctions. L'inverse n'est pas vrai.

Cf. rectangle.go

```go
package main

import "fmt"

type Rectangle struct {
  a, b int
}

// le param ici est appelé argument récepteur qui transforme 'Area' en méthode
func (r Rectangle) Area() int {
  return r.a * r.b
}

func main() {
  r := Rectangle{2, 3}
  fmt.Println(r.Area())
}
```

## Pointer

Un argument passé dans une fonction est copié dans le scope de la fonction.
Pour changer l'argument, le pointer vers la variable doit être transmis.

```go
package main
import "fmt"
func increase(i int) {
    i = i + 1
    fmt.Println(i)
}
func main() {
    i := 0
    fmt.Println(i)  // 0
    increase(i)     // 1
    fmt.Println(i)  // 0
}
```
Utilisation des pointers

```go
package main
import "fmt"

// la fonction attend un pointeur
func increase(i *int) {
  // on travaille sur les valeurs pointées
  *i = *i + 1
}

func main() {
    i := 0
    fmt.Println(i)

    // on transmet le pointeur vers la variable
    increase(&i)
    fmt.Println(i)
}
```

### Pointeurs sur les méthodes

```go
package main
import "fmt"

type Rectangle struct {
    a, b int
}

func (r *Rectangle) doubleIt() {
    r.a *= 2
    r.b *= 2
}

func main() {
    r := Rectangle{3, 4}
    fmt.Println(r.a, r.b) // 3 4
    r.doubleIt()
    fmt.Println(r.a, r.b) // 6 8
}
```