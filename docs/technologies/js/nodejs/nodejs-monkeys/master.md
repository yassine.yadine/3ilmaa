# Node.js server

## Ressources du cours

- [Playlist Youtube @simon_dieny](https://www.youtube.com/watch?v=Vz9RqPiyUo8&list=PLhVogk7htzNiO_cbnPqa7fkVzzRhUqQLj)
- 

## NPM -> Node Package Manager

***

### Commandes générales

***

``` bash
# Créer le fichier 'package.json'
npm init

# installer un package
npm install <nom_du_package>

# lister les packages installés
npm list

# supprimer un package
npm remove <nom_du_package>
```

### Placer un raccourci

***

Dans le fichier `package.json`, trouvez la section `scripts`

``` json
  "scripts": {

    // avec la ligne suivante, il sera possible de lancer la commande `npm run start`
    "start": "node src/app.js"
  },
```

***