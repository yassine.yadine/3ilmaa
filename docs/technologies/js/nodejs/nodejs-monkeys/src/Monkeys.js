const monkeys = [
    {
        id: 1,
        name: "gorgozor",
        age: 23
    },{
        id: 2,
        name: "sakinouch",
        age: 23
    },{
        id: 4,
        name: "amiruss",
        age: 23
    },{
        id: 5,
        name: "choupichou",
        age: 23
    },{
        id: 13,
        name: "swissine",
        age: 23
    }
]

module.exports = monkeys;
