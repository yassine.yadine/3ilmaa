 # Angular testing

 ## Tables des matières

 ***

- [Angular testing](#angular-testing)
  - [Tables des matières](#tables-des-matières)
  - [Ressources du cours](#ressources-du-cours)
  - [Jasmine \& Karma](#jasmine--karma)
    - [Jasmine](#jasmine)
    - [Karma](#karma)
  - [Installation](#installation)
  - [Structure projet](#structure-projet)
  - [Structure du fichier de test](#structure-du-fichier-de-test)
  - [Les matchers ou opérateurs de comparaison](#les-matchers-ou-opérateurs-de-comparaison)
  - [Isolement de cas de test](#isolement-de-cas-de-test)
    - [First describe 'f'](#first-describe-f)
    - [eXclude 'x'](#exclude-x)
  - [Injection de service \& Spy](#injection-de-service--spy)
  - [Couverture de test](#couverture-de-test)
  - [Test avec des composants enfants \& dépendances](#test-avec-des-composants-enfants--dépendances)
    - [Import de la dépendance :](#import-de-la-dépendance-)
    - [Eviter les erreurs de dépendances :](#eviter-les-erreurs-de-dépendances-)
  - [Tester un composant fils](#tester-un-composant-fils)
    - [Test si le composant fils a bien été chargé dans le DOM](#test-si-le-composant-fils-a-bien-été-chargé-dans-le-dom)
    - [Test sur un composant fils \& @Input](#test-sur-un-composant-fils--input)
    - [Test sur un composant fils \& @Output](#test-sur-un-composant-fils--output)


## Ressources du cours
 - [Playlist YT 'Coulisses learn'](https://www.youtube.com/playlist?list=PLckzBDi8b8tn-m414l0Hdoh0zNJta_bPa)
 - https://jasmine.github.io/
 - https://karma-runner.github.io/latest/index.html

 ## Jasmine & Karma

***

### Jasmine

***

C'est un framework pour développer des tests unitaires

### Karma

***

Fournit un environnement où seront exécuter les tests unitaires pour simuler par exemple le navigateur etc... Le projet Angular gère la conf Karma mais il est possible de customiser cette conf en générant un fichier.

```shell
ng generate config karma
```

Karma gère plusieurs framework de test.

Karma gère aussi plusieurs navigateurs que l'on appelle launcher.

## Installation

***

```bash
# installation d'angular cli
npm install -g @angular/cli

# création du projet
ng n unit-test --defaults --skip-git --directory=.
```

## Structure projet

***

Dans le fichier angular.json, il y a la confiuration du projet Angular dont on peut aussi trouver la config de test qui pointe vers le fichier de config Karma 'tsconfig.spec.js'

## Structure du fichier de test

***

```typescript
import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    
    // ici on configure le module contenant les composants à tester
    await TestBed.configureTestingModule({ 

    // ici on déclare le ou les composants que l'on souhaite tester
    // généralement c'est un composant et il a le même nom que le describe
      declarations: [
        AppComponent
      ],

      // pour importer des modules utilisés dans notre composants
      imports: []

      // cette fonction est optionnelle et ne sert que pour compiler des sources externes.
    }).compileComponents();
  });

    // it marque le début du cas de test
  it('should create the app', () => {

    // ici on crée un mirroir du composant que l'on souhaite tester
    const fixture = TestBed.createComponent(AppComponent);

    // ici nous récupérons l'instance du composant simulé
    const app = fixture.componentInstance;

    // assertion de test
    expect(app).toBeTruthy();
  });
});
```

Version amélioré du fichier de test

```typescript
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  // le mirroir du composant
  let fixture: ComponentFixture<AppComponent>;

  // instance du composant
  let app: AppComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    });

    // affectation des variables dans le beforeEach
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have as title 'unit-test'`, () => {
    expect(app.title).toEqual('unit-test');
  });

  it('should render title', () => {
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('unit-test app is running!');
  });
});
```

## Les matchers ou opérateurs de comparaison

***

[Voir la documentation Jasmine sur les matchers](https://jasmine.github.io/api/edge/matchers.html)

Il est aussi possible d'implémenter un matcher personnalisé.

## Isolement de cas de test

***

### First describe 'f'

```typescript
// en rajoutant la lettre 'f' juste avant it, jasmine ne vas exécuter que celui là
// c'est idéal pour se concentrer sur ce test uniquement
fit('should create the app', () => {
    expect(app).toBeTruthy();
  });
```

Il est possible d'avoir plusieurs describe dans le même fichier de test pour tester plusieurs perspectives par exemple. Il est ici possible de rajouter un 'f' au describe pour n'exécuter que ce module de test pour l'isoler.

### eXclude 'x'

***

```typescript
// en rajoutant la lettre 'x' juste avant it, jasmine ne vas pas l'exécuter
xit('should create the app', () => {
    expect(app).toBeTruthy();
  });
```

## Injection de service & Spy

***

Déclaration du service

```typescript
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;

  // déclaration du service
  let service: ComputeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    });

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

    // injection du service avec TestBed
    service = TestBed.inject(ComputeService);
  });
```

Cas de test de la méthode du service

```typescript
it('should verify that isValidNumber have been called', () => {
  // déclaration du spy avec le nom de la m&thode que l'on souhaite espionner
  let spyIsValidNumber: jasmine.Spy;

  // affectation du spy avec le nom de la méthode à mocker et son résultat mocké
  spyIsValidNumber = spyOn(service, 'isValidNumber').and.returnValue(true);

  // appel du composant ayant besoin du service
  app.calc(4,5);

  // assertion possible sur le spy
  expect(spyIsValidNumber).toHaveBeenCalledTimes(2);
});
```

Cas de test sur une propriété avec accesseur

```typescript
it('should verify that isValidNumber have been called', () => {
      spyIsValidNumber = spyOn(service, 'isValidNumber').and.returnValue(true);
      
      const spyValue: jasmine.Spy = spyOnProperty(service, 'value', 'set');;
      app.calc(4,5);

      expect(spyIsValidNumber).toHaveBeenCalledTimes(2);
      expect(spyValue).toHaveBeenCalled();
    });
```

## Couverture de test

***

Dans le fichier 'package.json', il faut rajouter ce paramètre à l'exécution du test

```json
"scripts": {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build",
    "watch": "ng build --watch --configuration development",

    // il faut rajouter --code-coverage
    "test": "ng test --code-coverage"
  },
```

Lors de la prochaine exécution des tests, un dossier à la racine du projet sera crée avec un site Web pour visualiser la couverture de test du projet dans sa globalité.

## Test avec des composants enfants & dépendances

***

Lorsque l'on teste un composant ayant des composants fils ou ayant des dépendances, il faut aussi les déclarer d'une certaine manière.

### Import de la dépendance :

Toutefois, ici il faudra déclarer autant de dépendances que nécessaire, ce qui peut-être fastidieux en phase de développement où les dépendances peuvent changer rapidement.

```typescript
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,

        // rajoutez ici le composant fils
        ChildComponent
      ]
    });
```

### Eviter les erreurs de dépendances :

```typescript
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],

      // rajoutez cette section
      schemas: [NO_ERRORS_SCHEMA]
    });
```

Toutefois, dans ce cas, il est difficile de savoir si les composants fils ont bien été chargés.

## Tester un composant fils 

***

### Test si le composant fils a bien été chargé dans le DOM

***

L'objectif n'est pas de remplacer les tests d'intégration mais de vérifier que le composant fils est bien chargé dans le DOM.

```typescript
  it('should check if app-accounting is present', () => {

    // ici nous récupérons tous les éléments HTML du DOM du composant à tester
    const elements = fixture.debugElement;

    // nous fesons une requête css pour récupérer le composant fils
    const accounting = elements.query(By.css('app-accounting'));

    // enfin, nous checkons que le composant a bien été chargé en mémoire
    expect(accounting).toBeTruthy();
  });
```

### Test sur un composant fils & @Input

***

```typescript
it('should add input data binding correctly', () => {
    // ici nous récupérons tous les éléments HTML du DOM du composant à tester
    const elements = fixture.debugElement;

    // nous fesons une requête css pour récupérer le composant fils
    const accounting = elements.query(By.css('app-accounting'));

    // on demande à Angular de détecter les changements dans le DOM
    fixture.detectChanges();

    // on teste l'input en allant chercher la property
    expect(accounting.properties['amount']).toBe(4);    
  });
```

### Test sur un composant fils & @Output

***

```typescript
  it('should add output data has changed correctly', () => {
    expect(app.currentAmount).toBe(0);

    // ici nous récupérons tous les éléments HTML du DOM du composant à tester
    const elements = fixture.debugElement;

    // nous fesons une requête css pour récupérer le composant fils
    const accounting = elements.query(By.css('app-accounting'));

    // on demande à Angular de simuler l'event-emitter et on lui associe une valeur
    accounting.triggerEventHandler('amountChanged', 10);

    expect(app.currentAmount).toBe(10);    
  });
```
