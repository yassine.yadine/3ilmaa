import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.component.html',
  styleUrls: ['./accounting.component.css']
})
export class AccountingComponent {

  @Input() amount!: number;

  @Output() amountChanged: EventEmitter<number> = new EventEmitter<number>();

}
