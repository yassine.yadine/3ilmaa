import { Component } from '@angular/core';
import { ComputeService } from './services/compute/compute.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'unit-test';
  age: number = 1;
  currentAmount: number = 0;
  
  constructor(private compute: ComputeService) {

  }

  changeAge() {
    this.age = 73;
  }

  calc(a: number, b: number): number {
    if (this.compute.isValidNumber(a) && this.compute.isValidNumber(b)) {
      this.compute.value  = a;
      return this.multiply(a, b);      
    }
    throw new Error('Sorry it is not a valid number');
  }

  private multiply(a: number, b: number): number {
    return a * b;
  }

  public currentChange(amount: number): void {
    this.currentAmount = amount;
    console.log('New amount: ' + amount);
  }
}
