import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './pokemon/page-not-found/page-not-found.component';

/**
 *  cette variable permet de déclarer les routes de l'application
 * on déclare les routes de haut en bas, de la plus spécifique à la plus générale
 */
const routes: Routes = [
  // déclaration de l'url vide, par défault
  {path: '', redirectTo: 'pokemons', pathMatch: 'full'},

  // permet d'intercepter toutes les routes
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
