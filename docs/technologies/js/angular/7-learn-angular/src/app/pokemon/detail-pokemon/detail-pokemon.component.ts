import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pokemon } from '../pokemon';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styles: [
  ]
})
export class DetailPokemonComponent implements OnInit {

  pokemon: Pokemon|undefined;
  constructor(private route: ActivatedRoute,
              private router: Router,
              private pokemonService: PokemonService) {}

  ngOnInit(): void {
    // permet de récupérer l'id de la route courante
    const pokemonId: string|null = this.route.snapshot.paramMap.get('id');
    
    if (pokemonId) {
      this.pokemon = this.pokemonService.getPokemonById(+pokemonId);
    }
  }

  goBack() {
    this.router.navigate(['/pokemons']);
  }

  goToEdition(pokemonId: number) {
    this.router.navigate(['/edit/pokemon', pokemonId]);
  }
}
