# Apprendre Angular

## Tables des matières

- [Apprendre Angular](#apprendre-angular)
  - [Tables des matières](#tables-des-matières)
  - [Ressources](#ressources)
  - [Interpolation](#interpolation)
    - [Directives d'attribut](#directives-dattribut)
    - [Property \& Event binding](#property--event-binding)
    - [Two way data binding](#two-way-data-binding)
  - [Cycle de vie ou hooks](#cycle-de-vie-ou-hooks)
  - [Les pipes](#les-pipes)
  - [Les routes](#les-routes)
    - [Définition des routes app-routing.module.ts](#définition-des-routes-app-routingmodulets)
    - [Appel à l'outlet router](#appel-à-loutlet-router)
    - [Gestion des routes dans le composant](#gestion-des-routes-dans-le-composant)
  - [Les modules](#les-modules)
    - [Structure du module](#structure-du-module)
    - [Exemple d'un sous module](#exemple-dun-sous-module)
    - [Déclaration du sous-module dans le module racine](#déclaration-du-sous-module-dans-le-module-racine)
  - [Les Services](#les-services)
    - [Structure d'un service](#structure-dun-service)
    - [Contrôle de l'injection de dépendance du service](#contrôle-de-linjection-de-dépendance-du-service)
  - [Les Formulaires](#les-formulaires)
    - [Déclaration du module FormsModule](#déclaration-du-module-formsmodule)
    - [Exemple de formulaire HTML](#exemple-de-formulaire-html)
    - [Exemple de logic pour la gestion d'un formulaire](#exemple-de-logic-pour-la-gestion-dun-formulaire)
  - [La Programmation Réactive](#la-programmation-réactive)
  - [Les Requêtes HTTP](#les-requêtes-http)
  - [La Libraririe RxJS](#la-libraririe-rxjs)
  - [Authentification et Sécurité](#authentification-et-sécurité)
  - [Déployer votre Application Angular](#déployer-votre-application-angular)
  - [\[BONUS\] Le JavaScript Moderne avec ES6.](#bonus-le-javascript-moderne-avec-es6)
  - [Commandes](#commandes)

## Ressources

***

- [cours suivi pour ce cours](https://www.youtube.com/watch?v=DTIYVffhJuU)
- [Apprendre Angular | Introduction à Angular et concepts avancés](https://www.youtube.com/playlist?list=PLrbLGOB571zeR7FUQifKmjUpT4ImldCPt)
- [Documentation Bootstrap](https://getbootstrap.com/docs/4.5/getting-started/introduction/)
- [Programmation réactive](https://rxmarbles.com/)
- [Projet RxJS](https://rxjs.dev/)

## Interpolation

***

C'est le fait d'interpréter une variable ou expression depuis le DOM.

![Interpolation](img/interpolation)

```html
 <!-- afficher une variable s'il n'est pas undefined -->
 <p>{{ selectedPokemon?.name }}</p>

 <!-- écouter la touchée entrée et la triggé depuis Angular -->
 <input 
    #input 
    (keyup.enter)="selectPokemon(input.value)"
    type="number">


  <p *ngIf="!selectedPokemon">Ce pokémon n'existe pas :/ {{ input.value }}</p>
```

### Directives d'attribut

***

Créer une directive
```bash
ng n directive directive-name
```

Exemple d'une directive qui change la couleur de bordure au survol d'un pokemon
```typescript
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {

  constructor(private el: ElementRef) { 
    this.setColor(this.initialColor);
    this.setHeight(this.defaultHeight);
  }

  initialColor: string = '#f5f5f5';
  defaultColor: string = '#009688';
  defaultHeight: number = 180;

  // input avec alias afin de décoréler le nom de la directive et la propriété dans le code
  @Input('appBorderCard') borderColor: string;

  // permet de gérer lorsque la souris est au-dessus du composant depuis où est appelé la diretive
  @HostListener('mouseenter') onMouseEnter() {
    this.setColor(this.borderColor || this.defaultColor);
  }

  // permet de gérer lorsque la souris n'est plus au-dessus du composant
  @HostListener('mouseleave') onMouseLeave() {
    this.setColor(this.initialColor);
  }

  setHeight(height: number) {
    // on récupère ici l'élément natif du DOM pour le modifier
    this.el.nativeElement.style.height = height + 'px';
  }

  setColor(color: string) {
    let border = 'solid 4px ' + color;
    this.el.nativeElement.style.border = border;
  }
}

```

Appel de la directive dans le code HTML
```html
<!-- appel de la directive en lui transmettant la couleur en paramètre -->
<div class="card horizontal" appBorderCard="#fc0341">
```

### Property & Event binding

***

### Two way data binding

***

## Cycle de vie ou hooks

***

![](img/hooks)

## Les pipes

***

Les pipes sont utilisés pour formater un contenu jugé trop brut comme les dates techniques par exemple. Cela permet de centraliser dans un fichier le rendu que l'on souhaite afficher à l'utilisateur.

Exemple d'un pipe

```typescript
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pokemonTypeColor'
})
export class PokemonTypeColorPipe implements PipeTransform {
  transform(type: string): string {
  
    let color: string;
  
    switch (type) {
      case 'Feu':
        color = 'red lighten-1';
        break;
      case 'Eau':
        color = 'blue lighten-1';
        break;
      case 'Plante':
        color = 'green lighten-1';
        break;
      case 'Insecte':
        color = 'brown lighten-1';
        break;
      case 'Normal':
        color = 'grey lighten-3';
        break;
      case 'Vol':
        color = 'blue lighten-3';
        break;
      case 'Poison':
        color = 'deep-purple accent-1';
        break;
      case 'Fée':
        color = 'pink lighten-4';
        break;
      case 'Psy':
        color = 'deep-purple darken-2';
        break;
      case 'Electrik':
        color = 'lime accent-1';
        break;
      case 'Combat':
        color = 'deep-orange';
        break;
      default:
        color = 'grey';
        break;
    }
  
    return 'chip ' + color;
  
  }
}

```

Appel d'un pipe dans le HTML

```html
<p><small>{{ pokemon.created | date: 'dd-MMM-yyyy' }}</small></p>
<span 
    *ngFor="let type of pokemon.types"
    class="{{ type | pokemonTypeColor }}">
    {{ type }}
</span>
```

## Les routes

***

### Définition des routes app-routing.module.ts

***

```typescript
/**
 *  cette variable permet de déclarer les routes de l'application
 * on déclare les routes de haut en bas, de la plus spécifique à la plus générale
 */
const routes: Routes = [
  // on associe une route à un composant
  {path: 'pokemons', component: ListPokemonComponent},
  {path: 'pokemon/:id', component: DetailPokemonComponent},

  // déclaration de l'url vide, par défault
  {path: '', redirectTo: 'pokemons', pathMatch: 'full'},

  // permet d'intercepter toutes les routes
  {path: '**', component: PageNotFoundComponent}
];
```

### Appel à l'outlet router

***

```html
<!-- cette balise Angular parmet d'injecter le composant qui match avec l'url courante -->
<router-outlet></router-outlet>
```

### Gestion des routes dans le composant

***

```typescript
  constructor(private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit(): void {
    this.pokemonList = POKEMONS;

    // permet de récupérer l'id de la route courante
    const pokemonId: string|null = this.route.snapshot.paramMap.get('id');
    
    if (pokemonId) {
      this.pokemon = this.pokemonList.find(poke => poke.id === +pokemonId);      
    }
  }

  goBack() {
    this.router.navigate(['/pokemons']);

    // redirection avec paramètre
    this.router.navigate(['/pokemon', pokemonId]);
  }
```

## Les modules

***

### Structure du module

***

```typescript
// le décorateur @NgModule prend en paramètre un objet ayant 5 propriétés
@NgModule({
  // declarations -> toutes les classes de vues (composant,pipe & directive) du module
  declarations: [
    AppComponent,
    BorderCardDirective,
    PokemonTypeColorPipe,
    ListPokemonComponent,
    DetailPokemonComponent,
    PageNotFoundComponent
  ],

  // exports -> classes du modules que l'on souhaite utilisables depuis l'extérieur
  exports: [],

  // imports -> classes dont nous avons besoin dans ce module
  imports: [
    BrowserModule,
    AppRoutingModule
  ],

  // providers -> services & gestion de dépendances
  providers: [],

  // bootstrap -> ne concerne que le module racine avec le nom du composant racine à charger au démarrage
  bootstrap: [AppComponent]
})
```

### Exemple d'un sous module

***

```typescript
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BorderCardDirective } from './border-card.directive';
import { PokemonTypeColorPipe } from './pokemon-types-color.pipe';
import { RouterModule, Routes } from '@angular/router';
import { DetailPokemonComponent } from './detail-pokemon/detail-pokemon.component';
import { ListPokemonComponent } from './list-pokemon/list-pokemon.component';

// déclaration de routes spécifiques au module
const pokemonRoutes: Routes = [
  {path: 'pokemons', component: ListPokemonComponent},
  {path: 'pokemon/:id', component: DetailPokemonComponent},
];

@NgModule({
  // on déclare dans notre module toutes les classes de vues nécessaires
  declarations: [BorderCardDirective,
                PokemonTypeColorPipe,
                DetailPokemonComponent,
                ListPokemonComponent],
  imports: [
    CommonModule,

    // prise en compte des routes spécifiques
    RouterModule.forChild(pokemonRoutes)
  ]
})
export class PokemonModule { }
```

### Déclaration du sous-module dans le module racine

***

```typescript
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  exports: [],
  imports: [
    BrowserModule,

    // déclaration ici du sous-module, avant le module de routage afin de privilégier nos routes de manières prioritaires
    PokemonModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
```

## Les Services

***

### Structure d'un service

***

```typescript
// avec cette annotation, le service pourra être injecté ailleurs
@Injectable({

  /**
   * cela permet de spécifier que l'on souhaite la même instance à travers toute l'application
   * angular utilisera l'injection de dépendance
  */ 
  providedIn: 'root'
})
```

### Contrôle de l'injection de dépendance du service

***

- Pour éviter que le service soit injecté dans toute l'appli, il faut supprimer la propriété 'providedIn' dans le service. 
- Ensuite, il faut déclarer le service dans le sous-module, ainsi il n'y aura qu'une seule instance du service viable uniquement dans le cycle de vie du module.

```typescript
// déclaration du service dans le module
@NgModule({
  declarations: [BorderCardDirective,
                PokemonTypeColorPipe,
                DetailPokemonComponent,
                ListPokemonComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(pokemonRoutes)
  ],
  providers: [PokemonService]
})
```

## Les Formulaires

***

### Déclaration du module FormsModule

***

Il faut déclarer l'import dans tous les modules ayant besoin de formulaires

```typescript
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  exports: [],
  imports: [
    BrowserModule,

    // ajout du FormsModule
    FormsModule,
    PokemonModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
```

### Exemple de formulaire HTML

***

```html
<!-- 
    on crée une variable dans le template HTML pokemonForm
    qui représente l'objet Angular ngForm pour gérer le 
    formulaire dans sa globalité.

    (ngSubmit) pour trigger la soumission du formulaire
 -->
<form *ngIf="pokemon" (ngSubmit)="onSubmit()" #pokemonForm="ngForm">
    <div class="row">
      <div class="col s8 offset-s2">
        <div class="card-panel">
    
          <!-- Pokemon name
        
          [(ngModel)]="pokemon.name" active le two way binding
      
          #name="ngModel" crée une variable dans le template
          pour gérer plus bas la validation de ce champ
          -->
          <div class="form-group">
            <label for="name">Nom</label>
            <input type="text" class="form-control" id="name"
                    required
                    pattern="^[a-zA-Z0-9àéèç]{1,25}$"
                   [(ngModel)]="pokemon.name" name="name"
                   #name="ngModel">
    
            <!-- on cache la div suivante si la variable 'name' est valid
              ou pristine -->
            <div [hidden]="name.valid || name.pristine"
                  class="card-panel red accent-1">
                  Le nom du pokémon est requis (1-25).
            </div>
          </div>
    
          <!-- Pokemon types -->
          <form class="form-group">
            <label for="types">Types</label>
            <p *ngFor="let type of types">
              <label>
                <input type="checkbox"
                  class="filled-in"
                  id="{{ type }}"
                  [value]="type"
                  [checked]="hasType(type)"
                  [disabled]="!isTypesValid(type)"
                  (change)="selectType($event, type)"/>
                <span [attr.for]="type">
                  <div class="{{ type | pokemonTypeColor }}">
                    {{ type }}
                  </div>
                </span>
              </label>
            </p>
          </form>
```

### Exemple de logic pour la gestion d'un formulaire

***

```typescript
Input() pokemon: Pokemon;
  types: string[];

  constructor(private pokemonService: PokemonService,
              private router: Router) {}

  ngOnInit(): void {
    this.types = this.pokemonService.getPokemonTypeList();
  }

  hasType(type: string): boolean {
    return this.pokemon.types.includes(type);
  }

  selectType(event: Event, type: string) {
    const isChecked = (event.target as HTMLInputElement).checked;

    if (isChecked) {
      this.pokemon.types.push(type);
    } else {
      const index = this.pokemon.types.indexOf(type);
      this.pokemon.types.splice(index, 1);
    }
  }

  isTypesValid(type: string): boolean {
    if (this.pokemon.types.length == 1 && this.hasType(type)) {
      return false;
    }

    if (this.pokemon.types.length > 2 && !this.hasType(type)) {
      return false;
    } 
    
    return true;
  }

  onSubmit() {
    console.log('Submit form !');
    this.router.navigate(['/pokemon', this.pokemon.id]);
  }
```

## La Programmation Réactive

***

Cf. cours sur la programmation réactive avec RxJS.

## Les Requêtes HTTP

***

## La Libraririe RxJS

***

## Authentification et Sécurité

***

## Déployer votre Application Angular

***

## [BONUS] Le JavaScript Moderne avec ES6.

***

## Commandes

***

```bash
# permet de voir le résultat de la commande sans faire les changements
ng g s pokemon/pokemon --dry-run
```