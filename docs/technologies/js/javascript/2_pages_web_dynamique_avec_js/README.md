# 7697016-Front-End.1

Code base du fil rouge pour le cours OpenClassrooms [Créez des pages web dynamiques avec JavaScript](https://openclassrooms.com/fr/courses/7697016-creez-des-pages-web-dynamiques-avec-javascript)

## Installation

Après avoir cloné le repo vous avez plusieurs options pour lancer le projet. 

Si vous utiliser VSCode ou un autre éditeur de code avec une extersion de serveur web comme live server, vous pouvez lancer direcement votre site avec l'extension que vous utilisez habituellement. 

Dans le cas contraire vous pouvez installer les dépendances de ce projet avec `npm install` puis lancer le projet via la commande `npm start`. Vous verrez dans le termninal le lien vers le site (par defaut http://127.0.0.1:8080 )

## Rappels

***

Appel du script JS dans le html

``` html
<!-- déclarer un script js dans le html -->
<script type="module" src="pieces.js"></script>
```

Le script JS appelé

``` javascript
/**
 * fichier 'pieces.js' 
 **/

// Récupération des pièces depuis le fichier JSON
// await permet de bloquer le traitement
const reponse = await fetch('pieces-autos.json');
const pieces = await reponse.json();

// instanciation de la variable 'article'
const article = pieces[4];

// instanciation des attributs que l'on va attcher au DOM
const imageElement = document.createElement("img");
imageElement.src = article.image;

const nomElement = document.createElement("h2");
nomElement.innerText = article.nom;

const prixElement = document.createElement("p");
prixElement.innerText = `Prix: ${article.prix} ${article.prix > 35 ? "€€€" : "€"}`;

const categorieElement = document.createElement("p");
categorieElement.innerText = article.categorie ?? "aucune catégorie à afficher";

// insertion de l'objet dans le DOM par le js
const sectionFiches = document.querySelector(".fiches");
sectionFiches.appendChild(imageElement);
sectionFiches.appendChild(nomElement);
sectionFiches.appendChild(prixElement);
sectionFiches.appendChild(categorieElement);

```

## Filtrer une liste d'objets

***

``` javascript
// ici on récupère le bouton html et on lui affecte listener 'click'
const priceFilter = document.getElementById('priceFilter');
priceFilter.addEventListener('click', function() {
    console.log("Filtre par prix !");
    const piecesFiltrees = pieces.filter(function(p) {
        return p.prix <= 35;
    });
    console.log(piecesFiltrees);
});
```

### Trier une collection

***

``` javascript
const sortByPrice = document.getElementById('sortByPrice');
sortByPrice.addEventListener('click', function() {
    console.log("Tri par prix !");

    const pieceTrieesParPrix = Array.from(pieces);
    pieceTrieesParPrix.sort(function(a, b) {
        return a.prix - b.prix;
    });
    console.log(pieceTrieesParPrix);
});
```
### Les fonctions lambdas

***

``` javascript
// Fonction lambda
piece => piece.nom

// Fonction normale
function (piece) {
    return piece.nom;
}

// instanciation d'une variable à l'aide d'une fonction lambda
const noms = pieces.map(piece => piece.nom);
```

### Fonction splice pour supprimer un élément d'une liste

***

``` javascript
const pieceNomEtPrix = pieces.map(piece => piece.nom + " - " + piece.prix + " €");

for (let i = pieces.length - 1; i >= 0; i--) {

    // si la piece n'est pas disponible
    if (! pieces[i].disponibilite) {

        // alors elle sera supprimée de la liste
        pieceNomEtPrix.splice(i, 1);
    }
}
```

### Rendre les interactions dynamiques

***

Il est possible de simuler un rafraichissement de la page Web de manière artificielle en récupérant l'élément HTML et en lui affectant une chaîne de caractère vide.

``` javascript
const eltHtml = document.getElementById('idDeLObjet');
eltHtml.innerHTML = ''; // l'élément sera nettoyé dans la vue HTML
```

### Créer un bouton dynamiquement

***

``` javascript
    const avisBouton = document.createElement("button");

    // on crée un attribut 'id' au bouton html auquel on lui affecte la valeur de la piece.id
    avisBouton.dataset.id = piece.id;
    avisBouton.textContent = "Afficher les avis";
```

### Appel à un service web 'fetch(GET)'

***

``` javascript
    // on récupère la valeur de l'id affecté au bouton précédent
    const id = event.target.dataset.id;

    // appel d'une API REST
    const feedbacks = fetch(`http://localhost:8081/pieces/${id}/avis`);
    console.log(feedbacks);

```

## Rappel sur les listeners

***

### Créer un listener dynamiquement

***

``` javascript
/**
 *  ce bout de code peut être ajouté dans un fichier '.js' contenant par exemple que des
 * listeners. Ici c'est un listener pour écouter un formulaire
**/
export function ajoutListenerEnvoyerAvis() {
  const formulaireAvis = document.querySelector(".formulaire-avis");

  // l'objet formulaire se trouve dans le paramètre event
  formulaireAvis.addEventListener("submit", function (event) {
    /* business logic here */
  });
}
```

### Import du listener

***

``` javascript
/**
 * Maintenant, il faut importer le listener dans le module chargé dans le HTML
 * 
**/
import { ajoutListenersAvis } from "./avis.js";

// appel de la fonction dans le module pour charger le listener
ajoutListenersAvis();

// A présent le listener est chargé
```

### Requête POST

***

``` javascript
// Création de l’objet du nouvel avis.
    const avis = {
      pieceId: parseInt(event.target.querySelector("[name=piece-id]").value),
      utilisateur: event.target.querySelector("[name=utilisateur").value,
      commentaire: event.target.querySelector("[name=commentaire]").value,
    };

    // Création de la charge utile au format JSON
    const chargeUtile = JSON.stringify(avis);

    // Appel de la fonction fetch avec toutes les informations nécessaires
    fetch("http://localhost:8081/avis", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: chargeUtile
    });
```

## Choisir une librairie

***

### Critère de sélection

***

- nombre de téléchargements, avis, ranking
- nombre de commits, fréquence, est-ce que le site est maintenue
- documentation présente ?
- le support technique
- les licences
  - MIT -> https://fr.wikipedia.org/wiki/Licence_MIT
  - Apache -> https://fr.wikipedia.org/wiki/Licence_Apache
  - GPL -> https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU

### Où trouver une librairie

***

- MDN -> https://developer.mozilla.org/fr/
- NPM -> https://npmjs.com/
- github.com

## Qualité du code

***

### Linter

***

``` bash
# installer le package eslint
npm install eslint

# créer le fichier de config où l'on va consigner les règles de qualité à suivre
npm init @eslint/config
# ici le linter va poser des questions auxquelles il faudra répondre

# exécuter le linter
npm run lint

```