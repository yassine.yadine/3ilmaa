## Ressources

- Source du cours -> https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript
- Emulateur JavaScript -> https://jsbin.com/?js,console
- Tableau de compatibilité JS -> https://kangax.github.io/compat-table/es6/
- ECMAScript 6 -> ensemble de normes concernant les langages de programmations de type Script (JavaScript, ActionScript, TypeScript)

## Part I. Données & type de données

### Déclaration & affectation de variables. On utilise donc le mot-clef 'let'.

```javascript
let numberOfCats = 2;
let numberOfDogs = 4;
```

### Déclaration de constantes

```javascript
const nombrePostParPage = 20;

nombrePostParPage = 30; // Retournera une erreur dans la console car on ne peut plus changer sa valeur
```

### Les types primitifs

Pas de déclaration spécifique pour les variables de type primitives.

- number
- boolean
- string

### Les objets JS

les objets sont déclarés en JSON (JavaScript Object Notation).

```javascript
let myBook = {
	title: 'The Story of Tau',
	author: 'Will Alexander',
	numberOfPages: 250,
	isAvailable: true
};
console.log(myBook.title); //will return 'The Story of Tau'
console.log(myBook["title"]);//will return 'The Story of Tau'
```

### Les classes JS

```javascript
class Book {
  constructor(title, author, pages){
    this.title = title;
    this.author = author;
    this.pages = pages;
  }
}

let myBook = new Book('Le château de ma mère', "Marcel Pagnol", 213);

console.log(myBook.title); // -> "Le château de ma mère"
console.log(myBook.author); // -> "Marcel Pagnol"
console.log(myBook.pages); // -> 213
```

### Les tableaux JS

#### Déclaration et affectation

```javascript
let guests = []; //tableau vide
let guests = ["Sarah Kate", "Audrey Simon", "Will Alexander"];
```

#### Accès aux éléments du tableau JS

On commence à 0. 

```javascript
let firstGuest = guests[0]; // "Sarah Kate"
let thirdGuest = guests[2]; // "Will Alexander"
let undefinedGuest = guests[12] // undefined
```

#### taille du tableau JS

```javascript
let guests = ["Will Alexander", "Sarah Kate", "Audrey Simon"];
let howManyGuests = guests.length; // 3
```

#### Ajout & suppression dans le tableau

```javascript
guests.push("Tao Perkington"); // ajoute "Tao Perkington" à la fin de notre tableau guests
guests.unshift("Tao Perkington"); // "Tao Perkington" est ajouté au début du tableau guests
guests.pop(); // supprimer le dernier élément du tableau
```

## Part II. Exécutez du JavaScript facilement

### Import script JS dans du HTML

```html
<script src="./script.js"></script>"
```

### Opérations

#### Switch

```javascript
switch (firstUser.accountLevel) {
	case 'normal':
      console.log('You have a normal account!');
	break;
	case 'premium':
      console.log('You have a premium account!');
	break;
	case 'mega-premium':
      console.log('You have a mega premium account!');
	break;
	default:
      console.log('Unknown account type!');
}
```

#### For

```javascript
for (let i = 0; i < passengers.length; i++) {
   console.log("Passager embarqué !");
}

const passengers = [
"Will Alexander",
"Sarah Kate'",
"Audrey Simon",
"Tao Perkington"
]
for (let i in passengers) {
   console.log("Embarquement du passager " + passengers[i]);
}

for (let passenger of passengers) {
   console.log("Embarquement du passager " + passenger);
}
```

#### Try / Catch

```javascript
try {
	// code susceptible à l'erreur ici
} catch (error) {
	// réaction aux erreurs ici
}
```

### Méthodes

#### Méthodes d'instances

Elles s'utilisent comme en Java :

```javascript
class Person {
   constructor(owner, balance) {
      this.owner = owner;
      this.balance = balance;
   }

  isMajor(){
    return this.balance >= 18;
  }
}

let amir = new Person("Amir", 7);
let yassine = new Person("Yassine", 35);
let sakina = new Person();

console.log(amir.isMajor());// -> false
console.log(yassine.isMajor());// -> true
console.log(new Person().isMajor());// -> false
```

#### Méthodes statiques

```javascript
class BePolite {    
	static sayHello() {
      console.log("Hello!");
	}

	static sayHelloTo(name) {
      console.log("Hello " + name + "!");
	}

	static add(firstNumber, secondNumber) {    
		return firstNumber + secondNumber;
	}
}

BePolite.sayHello(); // imprime "Hello!""
BePolite.sayHelloTo("Will"); // imprime "Hello Will!""
const sum = BePolite.add(2, 3); // sum = 5
```

### Tests

Voir les projets Jasmine, Mocha & mocha.js

### Debug

Voir Visual Studio.
