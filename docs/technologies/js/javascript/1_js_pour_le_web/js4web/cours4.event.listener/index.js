const parentElt = document.getElementById('parent');
let index = 0;
parentElt.addEventListener('click', function(){
  let parentCount = document.getElementById("parent-count");
  index++;
  parentCount.innerHTML = "<span id='parent-count'>".concat(index, '</span>');
});

const childElt = document.getElementById('child');
let childIndex = 0;
childElt.addEventListener('click', function(event){
	event.preventDefault();
	event.stopPropagation();
	let childCount = document.getElementById('child-count');
	childIndex++;
	childCount.innerHTML = "<span id='child-count'>".concat(childIndex,'</span>');

});


// version corrigée par openclassroom
/*
		let parentClicks = 0;
		let childClicks = 0;

		document
		  .getElementById("parent")
		  .addEventListener("click", function() {
		  document
		    .getElementById("parent-count")
		    .innerText = (++parentClicks) + '';
		});

		document
		  .getElementById("child")
		  .addEventListener("click", function(e) {
		  e.preventDefault();
		  e.stopPropagation();
		  
		  document
		    .getElementById("child-count")
		    .innerText = (++childClicks) + '';
		});
*/