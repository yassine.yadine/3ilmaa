document.addEventListener('DOMContentLoaded', function(){
	document.querySelector('select[name="ice-cream"]').onchange = changeEventHandler;
}, false);

function changeEventHandler(event) {
	// You can use "this" to refer to the selected element.
	if (! event.target.value) alert('Please select one');
	else alert('You like ' + event.target.value + ' ice cream.');
}

document.addEventListener('input', function(){
	document.querySelector('input').onchange = changeInputHandler;
}, false);

function changeInputHandler(event) {
	let sel = document.querySelector('option');
	sel.innerHTML = event.target.value; 
	console.log(sel);
}