
const btnApplyv2 = document.getElementById('btn-applyv2');

btnApplyv2.addEventListener('click', processCandidate);

async function processCandidate() {
    console.log('hi there!');
    try {
        const result = await startDecisionProcess();
        console.log(`result -> ${result}`);
    } catch (err) {
        console.log(`result -> ${err}`);
    }
    console.log('the process ended there');
}

console.log('une exécution en dehors du process v2');

const candidatv2 = {
    isProgrammer : false,
    isCool : true
}

function startDecisionProcess() {
    console.log('the recuiter is processing others candidates');
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (candidatv2.isProgrammer && candidatv2.isCool) {
                console.log('just before resolve');
                resolve('ok that sounds good for the future');
            } else {
                console.log('just before resolve');
                reject('despite your background... you\'re not accepted');
            }
        }, 3000);
    })
}
