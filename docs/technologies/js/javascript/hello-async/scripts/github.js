const githubForm = document.getElementById('github-form');
const githubContent = document.getElementById('github-content');

githubForm.addEventListener('submit', lookForAccount);

// le paramètre e est le submit event
function lookForAccount(e) {
    
    // l'appel de la fucntion preventDefault permet de ne pas rafraôchir toute la page 
    // après la soumission du formulaire
    e.preventDefault();
    const accountName = githubForm.elements[0].value;
    console.log(`looks good ${accountName}`);

    fetch(`https://api.github.com/users/${accountName}`)
        .then(data => data.json())
        .then(jsonData => {
            console.log(jsonData);
            githubContent.innerHTML = `<pre><code></code>${JSON.stringify(jsonData, null, 4)}</pre>`;
        });
}