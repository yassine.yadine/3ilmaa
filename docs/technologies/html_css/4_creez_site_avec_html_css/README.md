# Créer votre site avec HTML5 & CSS3

## Ressources

***

- [Lien vers le cours](https://openclassrooms.com/fr/courses/1603881-creez-votre-site-web-avec-html5-et-css3)
- [Apprendre Flexbox en s'amusant](https://flexboxfroggy.com/#fr)
- [Apprendre CSS Grid en s'amusant](https://codepip.com/games/grid-garden/)

## Les basiques HTML5

***

### Les images

***

```html
<!-- Cas standard -->
<p>
Voici une très belle photo que j'ai trouvée sur Unsplash :<br>
<img src="images/paysage.jpg" alt="Photo de plage vue du dessus" />
</p>

<!-- Afficher une infobulle -->
<img 
    src="montagnes.png" 
    title="Alors, envie de vous balader n'est-ce pas ?" 
    alt="Chemin de randonnée au milieu des montagnes"
>

<!-- Afficher une miniature cliquable qui redirige vers une image plus grande -->
<p>Vous souhaitez voir l'image dans sa taille d'origine ? Cliquez dessus !
<br>
<a href="images/montagne.jpg">
    <img 
        src="images/montagne_mini.jpg" 
        alt="Chemin de randonnée au milieu des montagnes" 
        title="Cliquez pour agrandir" 
    >
</a>
</p>
```

## Mettre en forme les pages Web avec CSS3

***

### Import du CSS dans le HTML

***

#### Les basiques

```html
<head>
    <meta charset="utf-8">
    <title>Ma page</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
    <h1>Un titre h1</h1>
</body>
```

```css
/* h1 -> sélecteur pour toutes les balises HTML.Cf. ressources */
h1 {
    color: blue; /* color -> propriété CSS. Cf. ressources */
}
```

```css
/* appliquer du css à plusieurs balises CSS en même temps */
h1, p
{
    color: blue;
}
```

#### Class & ID

Les attributs class et id fonctionnent selon la même méthode mais on ne les utilise pas pour les mêmes raisons :

- pour appliquer un style à un seul élément parmi d'autres, on utilise un attribut class.
- pour appliquer un style à un élément unique en son genre, on utilise un attribut id. 

En CSS, on peut appliquer du style à un élément (ou plus) avec l'attribut class. Par contre id ne peut s'utiliser que pour un seul élément, pas plus.

CLASS

```html
<body>
    <h1>Titre principal</h1>
    <p>Ceci est le contenu de mon premier paragraphe</p>
    <p class="ma-classe">Ceci est le contenu de mon deuxième paragraphe</p>
    <!-- multi classes -->
    <p class="ma-classe grand-texte">Ceci est le contenu de mon troisième paragraphe</p>
    <h2>Voilà mon sous-titre h2</h2>
</body>
```

```css
.ma-classe {
    color: #663399;
}
```

ID

```html
<img src="images/logo.png" alt="Logo du site" id="logo">
```


```css
#logo {
    /* code css; */
}
```

#### Les balises universelles

***

Il existe deux balises dites universelles qui ne servent à rien.
- span qui est une balise inline pour gérer du texte
- div qui est une balise block pour gérer des structures

### Gérer le texte

***

[liste de polices sur Google](https://fonts.google.com/)

```css
/*  taille de la police */
font-siez: 1em;

/* choix de la police */
font-family: 'Roboto', sans-serif;

/* mettre le texte en italique em | span */
font-syle: italic | normal;

/* souligner du texte */
text-decoration: underline; /* souligné */
text-decoration: line-through; /* barré */
text-decoration: none; /* normal (par défaut, sauf dans le cas des liens). */

/* algner un texte à droite, centre, gauche */
text-align: ;

```

### Ajoutez de la couleur et un fond

***

#### Les images en arrière plan

***

```css
/* super propriété backgroung */
background-image:;
background-repeat:;
background-attachment:  ;
background-size: ;
background-position:;
```

#### Les dégradés

***

```css
background: linear-gradient(90deg, #8360c3, #2ebf91);
```

Voir aussi [UI gradients](https://uigradients.com/) et [CSS Gradients](https://cssgradient.io/)

### Ombres & bordures

***

#### Les ombres

***

#### Les bordures

***

### Effets dynamiques

***

```css
/* hover est à utiliser quand l'utilisateur survole avec sa souris*/
a:hover, .ma-classe {
    // Insérer ici les propriétés CSS à appliquer
}

/* focus est à utiliser quand quand l'élément a le focus */
a:focus {
    background-color: #FCFFA6;
}

/* visited pour les éléments déjà visités */
a:visited {
    color: black;
}

/* le sélecteur universel sélectionnera toutes les balises sans exception */
* {
/* Insérez ici votre style */
}   
```

Le sélecteur d'une balise contenue dans une autre :

```html
<h3>Titre avec <em>texte important</em></h3>
```

```css
/* Applique ce style à toutes les balises  <em>  situées à l'intérieur d'une balise  <h3> */
h3 em {
    /* Insérez ici votre style */
}
```

Le sélecteur d'une balise qui en suit une autre :

```html
<h3>Titre</h3>
<p>Paragraphe</p>
```

```css
/* Ce qui aura pour résultat de sélectionner la première balise  <p>  située après un titre  <h3>. */
h3 + p {
/* Insérez ici votre style */
}
```

Le sélecteur d'une balise qui possède un attribut :

```html
<a href="http://site.com" title="Infobulle">
```

```css
/* Sélectionne tous les liens hypertexte   <a>  qui possèdent un attribut  title */
a[title] {
/* Insérez ici votre style */
}

/* Idem, l'attribut doit cette fois contenir dans sa valeur le mot “ici” (peu importe sa position). */
a[title*="ici"] {
/* Insérez ici votre style */
}
```

## Agencez le contenu de vos pages

***

## Utilisez des fonctionnalités avancées du HTML & CSS

***

### Structurez une page HTML

***

![](html_structure.png)

### Le modèle des boîtes

***



Vous pouvez aussi utiliser seulement les propriétés raccourcies  margin  ou padding en précisant quatre valeurs à la suite, dans le sens des aiguilles d'une montre (haut, droite, bas, gauche).

Exemple :  margin: 2px 0 3px 1px;.

Ce qui signifie :

“ 2 pixels de marge en haut, 0 pixel de marge à droite (le  px  est facultatif dans ce cas), 3 pixels de marge en bas et 1 pixel de marge à gauche”.

Aussi, il est possible de centre automatiquement les marges extérieures avec margin: auto;

![](html_margin_padding.png)

### Flexbox

***



```css
/* déclarer l'utilisation d'un bloc flex */
.container {
    display: flex;
}
```

Aligner les éléménts horizontalement avec justify-content

    - flex-start : Les éléments s'alignent au côté gauche du conteneur.
    - flex-end : Les éléments s'alignent au côté droit du conteneur.
    - center : Les éléments s'alignent au centre du conteneur.
    - space-between : Les éléments s'affichent avec un espace égal entre eux.
    - space-around : Les éléments s'affichent avec un espacement égal à l'entour d'eux.

Aligner les éléments verticalement avec align-items

    - flex-start : Les éléments s'alignent au haut du conteneur.
    - flex-end : Les éléments s'alignent au bas du conteneur.
    - center : Les éléments s'alignent au centre vertical du conteneur.
    - baseline : Les éléments s'alignent à la ligne de base du conteneur.
    - stretch : Les éléments sont étirés pour s'adapter au conteneur.

Inverser l'ordre des éléments avec flex-direction

    - row : Les éléments sont disposés dans la même direction que le texte.
    - row-reverse : Les éléments sont disposés dans la direction opposée au texte.
    - column : Les éléments sont disposés de haut en bas.
    - column-reverse : Les éléments sont disposés de bas en haut.

Dispatcher les éléments sur plusieurs lignes avec flex-wrap

    - nowrap : Tous les éléments sont tenus sur une seule ligne.
    - wrap : Les éléments s'enveloppent sur plusieurs lignes au besoin.
    - wrap-reverse : Les éléments s'enveloppent sur plusieurs lignes dans l'ordre inversé.

Déterminer l'espace entre les lignes

    - flex-start : Les lignes sont amassées dans le haut du conteneur.
    - flex-end: Les lignes sont amassées dans le bas du conteneur.
    - center : Les lignes sont amassées dans le centre vertical du conteneur.
    - space-between : Les lignes s'affichent avec un espace égal entre eux.
    - space-around : Les lignes s'affichent avec un espace égal autour d'eux.
    - stretch : Les lignes sont étirées pour s'adapter au conteneur.

! Il reste à faire l'exercice P3C3 dispo dans le dossier 'src'