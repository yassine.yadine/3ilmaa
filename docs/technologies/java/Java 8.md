# Java 8

## Les Streams

### Map

### Reduce

## Optionnal

## Programmation style fonctionnelle

##  Date & Time

## Parralel arrays

## Nashorn (JS engine)

## les Lambdas

L'objectif de la mise en place des lambas est de limiter l'usage des classes anonymes internes. Afin de rendre le code plus consis et plus lisible. Pour se faise, il faut fournir une interface fonctionnelle.

Les lambdas permettent l'interférence des types (le compilateur va devinier le type à la compilation, ainsi à l'exécution, il n'aura pas besoin de ressources CPU pour deviner le type).

### Les interfaces fonctionnelles

Une interface fonctionnelle est une interface avec une seule méthode. Ainsi, lors de l'appel de la lambdas, le compilateur sait exactement quelle méthode appeler.

### Les références de méthode

### Syntaxe 

```java
// Syntaxe #1
(param1, param2) -> expression;

// Syntaxe #2
(param1, param2) -> {
	corps
	};
```

Il faut chercher à privilégier la syntaxe #1. S'il faut plus de code, alors il faudrait utiliser les références de méthode.

### The dark side

Ressources :

- https://www.overops.com/blog/the-dark-side-of-lambda-expressions-in-java-8/