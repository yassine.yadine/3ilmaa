<map version="docear 1.1" project="176655C65742CX71RRQ5NM3E52RRKIFI8ZG1" project_last_home="file:/home/yassine/Developpement/projects/3ilmaa/" dcr_id="1642363478054_2bj1hpwuvoc75r2or44jsicdh">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="spring / BOOT" FOLDED="false" ID="ID_1094963657" CREATED="1642363374964" MODIFIED="1642446629533">
<hook NAME="AutomaticEdgeColor" COUNTER="5"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="framework pour d&#xe9;velopper des web-app" POSITION="right" ID="ID_792780117" CREATED="1642363747780" MODIFIED="1642363764283">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="modules" POSITION="right" ID="ID_1184827645" CREATED="1642363780410" MODIFIED="1642363806771" MOVED="1642363806777">
<edge COLOR="#0000ff"/>
<node TEXT="core" ID="ID_356455676" CREATED="1642363844194" MODIFIED="1642363847024"/>
<node TEXT="security" ID="ID_1036560402" CREATED="1642363815025" MODIFIED="1642363817633" MOVED="1642365243447">
<node TEXT="doc" ID="ID_409905857" CREATED="1642365246007" MODIFIED="1642365248119"/>
</node>
<node TEXT="boot" ID="ID_1542923613" CREATED="1642363818055" MODIFIED="1642363819805"/>
<node TEXT="data" ID="ID_511757005" CREATED="1642363820476" MODIFIED="1642363824775">
<node TEXT="jpa" ID="ID_908250229" CREATED="1642363826095" MODIFIED="1642363827911">
<node TEXT="doc" ID="ID_1124204782" CREATED="1642365254748" MODIFIED="1642365256460"/>
</node>
<node TEXT="ldap" ID="ID_1495908263" CREATED="1642363828509" MODIFIED="1642363830318"/>
</node>
</node>
<node TEXT="principes" POSITION="left" ID="ID_1192775855" CREATED="1642363866996" MODIFIED="1642363869829">
<edge COLOR="#00ff00"/>
<node TEXT="SOLID" ID="ID_1445720816" CREATED="1642363877992" MODIFIED="1642363890552">
<node TEXT="single responsability" ID="ID_1475412723" CREATED="1642363892159" MODIFIED="1642364176837">
<node TEXT="une classe doit &#xea;tre responsable que de son cas" ID="ID_1331315694" CREATED="1642364299532" MODIFIED="1642364312118"/>
</node>
<node TEXT="open / closed" ID="ID_82548095" CREATED="1642364167536" MODIFIED="1642364173318"/>
<node TEXT="liskov substitution" ID="ID_1467730523" CREATED="1642364178042" MODIFIED="1642364188889">
<node TEXT="si S est un sous-type de T&#xa;alors tout objet S peut remplacer T&#xa;Sans modifier le comportement du programme&#xa;&#xa;S (carr&#xe9;) -&gt; T(rectangle)" ID="ID_1899286418" CREATED="1642364517749" MODIFIED="1642364587917"/>
</node>
<node TEXT="interface segregation" ID="ID_1907955830" CREATED="1642364192435" MODIFIED="1642364205847"/>
<node TEXT="dependency inversion" ID="ID_138650635" CREATED="1642364228197" MODIFIED="1642364238195">
<node TEXT="l&apos;inversion de controle IoC est le fait de d&#xe9;l&#xe9;guer&#xa; &#xe0; une tierce partie, la cr&#xe9;ation et l&apos;injection d&apos;instances&#xa;dans d&apos;autres classes." ID="ID_740091588" CREATED="1642364242046" MODIFIED="1642364294481"/>
<node TEXT="IoC container&#xa;&#xa;Aussi appel&#xe9; context" ID="ID_1938310975" CREATED="1642364788331" MODIFIED="1642364803270"/>
</node>
</node>
</node>
<node TEXT="classes abstraites" POSITION="left" ID="ID_1974818797" CREATED="1642364216084" MODIFIED="1642364224670">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="doc" POSITION="right" ID="ID_1018101542" CREATED="1642365023911" MODIFIED="1642365059973" LINK="project://176655C65742CX71RRQ5NM3E52RRKIFI8ZG1/docs/technologies/java/spring/Spring.md">
<edge COLOR="#00ffff"/>
</node>
</node>
</map>
