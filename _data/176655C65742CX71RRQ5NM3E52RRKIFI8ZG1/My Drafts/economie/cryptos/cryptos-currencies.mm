<map version="docear 1.1" dcr_id="1612857044304_7jsef438hkklknaex595umi25" project="176655C65742CX71RRQ5NM3E52RRKIFI8ZG1" project_last_home="file:/C:/Users/60000215/Docear/projects/3ilmaa/">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="cryptos-currencies" FOLDED="false" ID="ID_572391160" CREATED="1612857044265" MODIFIED="1612857044268">
<hook NAME="AutomaticEdgeColor" COUNTER="5"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="plateformes" POSITION="right" ID="ID_1502750480" CREATED="1612857074788" MODIFIED="1612857077477">
<edge COLOR="#00ff00"/>
</node>
<node TEXT="proof of work" POSITION="right" ID="ID_611179970" CREATED="1612857077957" MODIFIED="1612857091383">
<edge COLOR="#ff00ff"/>
<node TEXT="meilleur compromis pour assurer le consensus&#xa;au sein du r&#xe9;seau pour assurer que le bloc&#xa;est valide" ID="ID_1793240060" CREATED="1612857077957" MODIFIED="1612975872592"/>
</node>
<node TEXT="proof of stake" POSITION="right" ID="ID_1847670638" CREATED="1612857091595" MODIFIED="1612857095951">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="binance" POSITION="left" ID="ID_1939697277" CREATED="1612863627915" MODIFIED="1612863635176">
<edge COLOR="#7c0000"/>
<node TEXT="GU7OJATJGGCMFBCQ" ID="ID_658218576" CREATED="1612863647137" MODIFIED="1612863807331"/>
</node>
<node TEXT="bilaxy" POSITION="left" ID="ID_1367506245" CREATED="1617185237591" MODIFIED="1617185246719">
<edge COLOR="#00ffff"/>
<node TEXT="SQAOYLJLSNGAG7C2" ID="ID_1320088574" CREATED="1617185246729" MODIFIED="1617185250529"/>
</node>
<node TEXT="minner" POSITION="left" ID="ID_532828689" CREATED="1612863641685" MODIFIED="1612965758691" MOVED="1612965751859">
<edge COLOR="#7c7c00"/>
<font ITALIC="false"/>
<node TEXT="valider / cr&#xe9;er de nouveeaux blocs" ID="ID_209386721" CREATED="1612863641685" MODIFIED="1612975934281" MOVED="1612965751859">
<font ITALIC="false"/>
</node>
</node>
<node TEXT="halving" POSITION="left" ID="ID_1031766135" CREATED="1612966565769" MODIFIED="1612966568395">
<edge COLOR="#ff0000"/>
<node TEXT="r&#xe9;duction de la r&#xe9;compense de&#xa;minnage au fil du temps" ID="ID_1407018936" CREATED="1612966565769" MODIFIED="1612966648786"/>
<node TEXT="&#xe0; chaque milestone, la r&#xe9;compense&#xa;est divis&#xe9;e par 2" ID="ID_1899493462" CREATED="1612973036039" MODIFIED="1612973056390"/>
</node>
<node TEXT="wallet/portefeuille" POSITION="left" ID="ID_23713720" CREATED="1612969637126" MODIFIED="1612969646101">
<edge COLOR="#00ffff"/>
<node TEXT="hot wallet / logiciel" ID="ID_1731933812" CREATED="1612969637126" MODIFIED="1612969658453"/>
<node TEXT="cold wallet / physique/usb" ID="ID_980131250" CREATED="1612969687719" MODIFIED="1612969699053"/>
<node TEXT="https://academy.binance.com/fr/articles/crypto-wallet-types-explained" ID="ID_646555289" CREATED="1612969820787" MODIFIED="1612969826988" LINK="https://academy.binance.com/fr/articles/crypto-wallet-types-explained"/>
</node>
<node TEXT="ICO" POSITION="left" ID="ID_1913436265" CREATED="1615210021387" MODIFIED="1615210023958">
<edge COLOR="#007c00"/>
</node>
<node TEXT="NFT" POSITION="left" ID="ID_932817679" CREATED="1615211161833" MODIFIED="1615211164418">
<edge COLOR="#7c007c"/>
</node>
<node TEXT="collectibles" POSITION="left" ID="ID_667965901" CREATED="1615211165228" MODIFIED="1615211168373">
<edge COLOR="#007c7c"/>
</node>
<node TEXT="fontgibles" POSITION="left" ID="ID_107161484" CREATED="1615211168954" MODIFIED="1615211179537">
<edge COLOR="#7c7c00"/>
<node TEXT="?" ID="ID_857776412" CREATED="1615224280082" MODIFIED="1615224283409"/>
</node>
<node TEXT="consensus ?" POSITION="left" ID="ID_1224453560" CREATED="1615224285554" MODIFIED="1615224290725">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="machine de Turin" POSITION="left" ID="ID_1197413665" CREATED="1615224537045" MODIFIED="1615224542987">
<edge COLOR="#0000ff"/>
</node>
<node TEXT="attaque 51" POSITION="left" ID="ID_645639989" CREATED="1615224543454" MODIFIED="1615224549971">
<edge COLOR="#00ff00"/>
</node>
<node TEXT="bizantines" POSITION="left" ID="ID_719136228" CREATED="1615224550510" MODIFIED="1615224563409">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="cryptos" POSITION="right" ID="ID_957936499" CREATED="1612975889223" MODIFIED="1612975892196">
<edge COLOR="#7c0000"/>
<node TEXT="protocoles" ID="ID_300163340" CREATED="1612857117731" MODIFIED="1612975895716" MOVED="1612975895720"/>
<node TEXT="yield farmer" ID="ID_1696191230" CREATED="1612968487419" MODIFIED="1612975899672" MOVED="1612975899675"/>
<node TEXT="defi" ID="ID_620302912" CREATED="1612857061667" MODIFIED="1612975949315" MOVED="1612975951676"/>
<node TEXT="storage" ID="ID_1566739456" CREATED="1612857070726" MODIFIED="1612975957409" MOVED="1612975957412"/>
</node>
<node TEXT="blockchain" POSITION="right" ID="ID_560836298" CREATED="1612976274707" MODIFIED="1612976279635">
<edge COLOR="#00007c"/>
<node TEXT="syst&#xe8;me d&#xe9;centralis&#xe9; &amp; distribu&#xe9;" ID="ID_894452209" CREATED="1612976274707" MODIFIED="1612976297793"/>
</node>
</node>
</map>
