<map version="docear 1.1" project="176655C65742CX71RRQ5NM3E52RRKIFI8ZG1" project_last_home="file:/C:/cimpa/perso/3ilmaa/" dcr_id="1614605578848_2jih4xv1bn4sduyyr78y3eqzm">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="trading" FOLDED="false" ID="ID_824547319" CREATED="1614605578814" MODIFIED="1614605578817">
<hook NAME="AutomaticEdgeColor" COUNTER="12"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="les m&#xe9;tiers" FOLDED="true" POSITION="left" ID="ID_1898775032" CREATED="1614605844914" MODIFIED="1614605853275">
<edge COLOR="#00ffff"/>
<node TEXT="trader" ID="ID_924401213" CREATED="1614605591542" MODIFIED="1614605860860">
<node TEXT="fait de l&apos;analyse mais prend une d&#xe9;cision &#xe0; la fin" ID="ID_894199029" CREATED="1614605591542" MODIFIED="1614605882693"/>
<node TEXT="il doit g&#xe9;rer le money management" ID="ID_1046506778" CREATED="1614605883068" MODIFIED="1614605898022"/>
<node TEXT="il doit g&#xe9;rer le risque" ID="ID_305693721" CREATED="1614605898358" MODIFIED="1614605906689"/>
<node TEXT="la diversification du portefeuille" ID="ID_1515138023" CREATED="1614605908143" MODIFIED="1614605916352"/>
<node TEXT="l&apos;exposition du portefeuille sur les diff&#xe9;rents march&#xe9;s" ID="ID_1070432604" CREATED="1614605930908" MODIFIED="1614605942911"/>
<node TEXT="quand rentrer/sortir de sa position" ID="ID_927247900" CREATED="1614606024351" MODIFIED="1614606033530"/>
</node>
<node TEXT="analyste" ID="ID_669162901" CREATED="1614605861156" MODIFIED="1614606013299" VSHIFT="80">
<node TEXT="&#xe9;tudie un actif &#xe0; un moment donn&#xe9;" ID="ID_1422651166" CREATED="1614605591542" MODIFIED="1614605959844"/>
<node TEXT="pr&#xe9;dit une baisse/hausse/range" ID="ID_759612521" CREATED="1614605960279" MODIFIED="1614605973095"/>
<node TEXT="fournit une synth&#xe8;se pr&#xe9;conisant ce qu&apos;il faut faire" ID="ID_1612218648" CREATED="1614605973520" MODIFIED="1614605990321">
<node TEXT="acheter" ID="ID_1056603848" CREATED="1614605591542" MODIFIED="1614605999815"/>
<node TEXT="vendre" ID="ID_979433232" CREATED="1614606000280" MODIFIED="1614606002225"/>
<node TEXT="ne rien faire" ID="ID_1420495161" CREATED="1614606003054" MODIFIED="1614606005832"/>
</node>
</node>
<node TEXT="courtier/broker" ID="ID_1568602328" CREATED="1615968904682" MODIFIED="1615968915264"/>
</node>
<node TEXT="social trading" POSITION="left" ID="ID_518922003" CREATED="1612543606464" MODIFIED="1612543730266">
<edge COLOR="#00ff00"/>
<node TEXT="copie les meilleurs traders" ID="ID_1441788086" CREATED="1612543606464" MODIFIED="1612543756305"/>
<node TEXT="copie stop loss" ID="ID_1517441088" CREATED="1612543756766" MODIFIED="1612543763738"/>
<node TEXT="rentrer en contact avec eux" ID="ID_1699876922" CREATED="1612543764110" MODIFIED="1612543770991"/>
</node>
<node TEXT="liquidity" POSITION="left" ID="ID_989640807" CREATED="1615455568951" MODIFIED="1615455575099">
<edge COLOR="#7c0000"/>
</node>
<node TEXT="le process" POSITION="right" ID="ID_1648463191" CREATED="1615455609372" MODIFIED="1615455616140">
<edge COLOR="#00007c"/>
<node TEXT="plan de trading" ID="ID_673156333" CREATED="1614605581335" MODIFIED="1615455622553"/>
<node TEXT="money management" ID="ID_1211337149" CREATED="1614605586618" MODIFIED="1615455622575">
<node TEXT="quelle est la partie de mon portefeuille &#xe0; engager ?" ID="ID_524215438" CREATED="1614605591542" MODIFIED="1614605684431"/>
<node TEXT="quelle est l&apos;exposition &#xe0; avoir ?" ID="ID_1326986907" CREATED="1614605685258" MODIFIED="1614605695964"/>
<node TEXT="quelle marge ?" ID="ID_1509090222" CREATED="1614605698906" MODIFIED="1614605703619"/>
</node>
<node TEXT="risk management" ID="ID_202581394" CREATED="1614605591542" MODIFIED="1615455622584"/>
</node>
<node TEXT="les march&#xe9;s" POSITION="right" ID="ID_407286556" CREATED="1615455630549" MODIFIED="1615455646004">
<edge COLOR="#007c00"/>
<node TEXT="le forex" ID="ID_1869193049" CREATED="1615455660232" MODIFIED="1615455758276" LINK="project://176655C65742CX71RRQ5NM3E52RRKIFI8ZG1/_data/176655C65742CX71RRQ5NM3E52RRKIFI8ZG1/My%20Drafts/economie/invest/forex.mm"/>
<node TEXT="les mati&#xe8;res premi&#xe8;res" ID="ID_1138757370" CREATED="1615456076828" MODIFIED="1615456083547"/>
<node TEXT="les cryptos" ID="ID_1401730540" CREATED="1615456084214" MODIFIED="1615456110875" LINK="project://176655C65742CX71RRQ5NM3E52RRKIFI8ZG1/_data/176655C65742CX71RRQ5NM3E52RRKIFI8ZG1/My%20Drafts/economie/cryptos/cryptos-currencies.mm"/>
<node TEXT="l&apos;or/l&apos;argent" ID="ID_1834322895" CREATED="1615456116735" MODIFIED="1615456127212"/>
<node TEXT="les futures" ID="ID_1590818422" CREATED="1615456127753" MODIFIED="1615456132797"/>
<node TEXT="les d&#xe9;riv&#xe9;s" ID="ID_910541676" CREATED="1615456133501" MODIFIED="1615456141579"/>
<node TEXT="pools" ID="ID_975606813" CREATED="1615456162339" MODIFIED="1615479716394"/>
</node>
<node TEXT="les indicateurs" POSITION="right" ID="ID_1877939278" CREATED="1615478179025" MODIFIED="1616193766750" LINK="project://176655C65742CX71RRQ5NM3E52RRKIFI8ZG1/_data/176655C65742CX71RRQ5NM3E52RRKIFI8ZG1/My%20Drafts/economie/invest/indicators.mm">
<edge COLOR="#007c7c"/>
<node TEXT="#1 - tendance" ID="ID_1613582085" CREATED="1616193290489" MODIFIED="1616193299165">
<node TEXT="SMA - Smoothed Moving MAverage" ID="ID_98090174" CREATED="1616193422903" MODIFIED="1616193497304"/>
<node TEXT="diff&#xe9;rence prix offre / prix demande" ID="ID_834767116" CREATED="1615479627352" MODIFIED="1615479644289"/>
<node TEXT="profondeur du carnet d&apos;ordres" ID="ID_1988343594" CREATED="1615479606483" MODIFIED="1615479626912"/>
</node>
<node TEXT="#2 - momentum" ID="ID_1622129769" CREATED="1616193299599" MODIFIED="1616193318122">
<node TEXT="RSI - Relative Strenght Indice" ID="ID_1213264719" CREATED="1615478199185" MODIFIED="1615478215986"/>
<node TEXT="Stockastique" ID="ID_158102057" CREATED="1615478216658" MODIFIED="1615478222901"/>
<node TEXT="macd" ID="ID_492105884" CREATED="1616193388599" MODIFIED="1616193394462"/>
</node>
<node TEXT="#3 - volume" ID="ID_1213339963" CREATED="1616193320141" MODIFIED="1616193362627">
<node TEXT="le volume 24h" ID="ID_46231944" CREATED="1615479596195" MODIFIED="1615479604205"/>
<node TEXT="cap market" ID="ID_793569339" CREATED="1616193406435" MODIFIED="1616193410155"/>
</node>
<node TEXT="#4 - volatilit&#xe9;" ID="ID_35829519" CREATED="1616193363696" MODIFIED="1616193372066">
<node TEXT="Fibonacci" ID="ID_1657580981" CREATED="1615478223355" MODIFIED="1615478231531"/>
<node TEXT="bande de Bollinger" ID="ID_483543686" CREATED="1616193539577" MODIFIED="1616193544629"/>
</node>
</node>
</node>
</map>
