<map version="docear 1.1" project="176655C65742CX71RRQ5NM3E52RRKIFI8ZG1" project_last_home="file:/C:/cimpa/perso/3ilmaa/" dcr_id="1612434594010_6ekt94d1ke80xlmm6kauyybn8">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="forex" FOLDED="false" ID="ID_1173867820" CREATED="1612434491356" MODIFIED="1612450140669" LINK="project://176655C65742CX71RRQ5NM3E52RRKIFI8ZG1/docs/invest.docs/Forex.docx">
<hook NAME="AutomaticEdgeColor" COUNTER="5"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="stable m&#xea;me en p&#xe9;riode de crise" POSITION="right" ID="ID_1378266001" CREATED="1612543522117" MODIFIED="1612543538051">
<edge COLOR="#ff0000"/>
</node>
<node TEXT="trad&#xe9;es en paire" POSITION="right" ID="ID_1874284690" CREATED="1612543544917" MODIFIED="1612543550424">
<edge COLOR="#0000ff"/>
<node TEXT="majors" ID="ID_1484354110" CREATED="1612543854937" MODIFIED="1612543883680">
<node TEXT="EUR/USD -&gt; 28% du march&#xe9;" ID="ID_446027504" CREATED="1612543606464" MODIFIED="1612543903299"/>
</node>
<node TEXT="others" ID="ID_827277772" CREATED="1612543883960" MODIFIED="1612543887830"/>
</node>
<node TEXT="plus grand march&#xe9; trad&#xe9;s" POSITION="right" ID="ID_667661800" CREATED="1612543606464" MODIFIED="1612543614680">
<edge COLOR="#00ff00"/>
<node TEXT="+5 trillon $" ID="ID_1776615040" CREATED="1612543606464" MODIFIED="1612543637322"/>
<node TEXT="bourse" ID="ID_236205460" CREATED="1612543638737" MODIFIED="1612543651381">
<node TEXT="NY -&gt; 22.4 billion" ID="ID_1713716055" CREATED="1612543606464" MODIFIED="1612543668024"/>
<node TEXT="Tokyo -&gt; 18.4 billion" ID="ID_669384197" CREATED="1612543668421" MODIFIED="1612543683205"/>
<node TEXT="Londres -&gt; 7.2 billion" ID="ID_712928435" CREATED="1612543683652" MODIFIED="1612543695072"/>
</node>
</node>
<node TEXT="4000 MM/jour de CA" POSITION="left" ID="ID_1456756528" CREATED="1612543779613" MODIFIED="1612543789258">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="il y a toujours un acheteur/vendeur" POSITION="left" ID="ID_511460548" CREATED="1612543793613" MODIFIED="1612543804007">
<edge COLOR="#00ffff"/>
</node>
</node>
</map>
